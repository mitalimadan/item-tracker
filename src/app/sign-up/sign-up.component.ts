import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {ItemServiceService} from '../itemService.service';


@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  signUpForm: FormGroup;

  constructor(private itemService: ItemServiceService) {

    this.signUpForm = new FormGroup({
      name: new FormControl(),
      email: new FormControl(),
      password: new FormControl(),
      type: new FormControl(),


    });
  }

  ngOnInit() {

  }


  signUpInfo() {
    const signUpInfo = this.signUpForm.value;
    this.itemService
      .signUpInfo(signUpInfo)
      .subscribe((response) => {
      alert('SignUp Successfully')
        console.log("hello");

    });

  }

}
