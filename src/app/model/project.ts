/**
 * Created by Lucky  on 12/05/2020.
 */

export class Project {
  id: number;
  name: string;
  description: string;
  owner: number;
  startedAt: Date;
  status: number;
}
