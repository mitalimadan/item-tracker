/**
 * Created by Lucky  on 11/05/2020.
 */
export class Item {
  id: number;
  name: string;
  description: string;
  parent: number;
  type: number;
  priority: number;
  createdAt: Date;
  createdBy: number;
  estimatedTime: number;
  projectLinked: number;
  createdByName:string;
  projectLinkedByName:string;
}
