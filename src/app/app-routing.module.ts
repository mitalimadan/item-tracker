import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ViewItemComponent} from './view-task/view-item.component';
import {AddItemComponent} from './add-task/add-item.component';
import {SignUpComponent} from './sign-up/sign-up.component';


const routes: Routes = [
  {path: 'view-item', component: ViewItemComponent},
  {path: 'add-item', component: AddItemComponent},
  {path: 'sign-up', component: SignUpComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
