import {TestBed} from '@angular/core/testing';

import {ItemServiceService} from './itemService.service';

describe('ItemServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ItemServiceService = TestBed.get(ItemServiceService);
    expect(service).toBeTruthy();
  });
});
